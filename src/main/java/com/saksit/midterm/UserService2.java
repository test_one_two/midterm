/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksit.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class UserService2 {
     private static ArrayList<User2> user2List = new ArrayList<>();;
     static{
         
     }
     // Create (C)
    public static boolean addUser(User2 user2){
        user2List.add(user2);
        save();
        return true;
  
    }
     // Delete (D)
    public static boolean delUser(User2 user2){
        user2List.remove(user2);
        save();
        return true;
    }
     public static boolean delUser(int index){
        user2List.remove(index); 
        save();
        return true;       
    } public static boolean delUser2(int index){
        user2List.removeAll(user2List);
        save();
        return true;       
    }
     // Read (R)
    public static ArrayList<User2> getUsers(){
         return user2List;
    }
    public static User2 getUser(int index){
        return user2List.get(index);
    }
    // Update  (U)
    public static boolean updateUser(int index,User2 user2){
        user2List.set(index,user2);
        save();
        return true;
    }
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
             file = new File("Bank.dat");
             fos = new FileOutputStream(file);
             oos = new ObjectOutputStream(fos);
             oos.writeObject(user2List);
             oos.close();
             fos.close();
         } catch (FileNotFoundException ex) {
             Logger.getLogger(UserService2.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(UserService2.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
     public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
             file = new File("Bank.dat");
             fis = new FileInputStream(file);
             ois = new ObjectInputStream(fis);
             user2List = (ArrayList<User2>)ois.readObject();
             ois.close();
             fis.close();
         } catch (FileNotFoundException ex) {
             Logger.getLogger(UserService2.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(UserService2.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(UserService2.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
}

