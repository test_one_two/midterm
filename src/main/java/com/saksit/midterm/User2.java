/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksit.midterm;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class User2 implements Serializable{
    private String name;
    private String brand;
    private String price;
    private String amount;

    public User2(String name, String brand, String price, String amount) {
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
   
    
    @Override
    public String toString() {
        
        return "  name  =  " + name + ",  brand  =  " + brand + ",  price  =  " + price + ",  amount  =  " + amount;
        
    }
    

}
